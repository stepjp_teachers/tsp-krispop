#!/usr/bin/perl -w

use strict;
use File::Basename;

my ($me, $dir) = fileparse($0);
$dir = "." unless $dir;
my $shinyProg = (`which toilet` ? 'toilet -f future --gay' : 'echo');
my $fromProg = "$dir/fromlkh.pl";
my $toProg = "$dir/tolkh.pl";
my $lkhProg = ($ENV{LKH} or 'LKH');
die "Missing $fromProg" unless -x $fromProg;
die "Missing $toProg" unless -x $toProg;
die "Missing LKH program" unless `which $lkhProg`;

foreach my $file (@ARGV) {
  shiny($file);
  system("$toProg $file && $lkhProg $file.lkh.par && $fromProg $file.lkh.res");
}

sub shiny {
  my ($file) = @_;
  my $text = ($file=~m/input_(\d+)\.csv$/) ? "Challenge $1":$file;
  die unless $text;
  system("$shinyProg ".quotemeta($text));
}
