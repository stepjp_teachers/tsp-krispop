#!/usr/bin/perl

use strict;
use Fatal qw(open close);

foreach my $file (@ARGV) {
  open(my $ifh, $file);
  open(my $ofh, ">$file.lkh.tsp");
  open(my $opfh, ">$file.lkh.par");
  my $head = <$ifh>;
  my @coords = <$ifh>;
  close $ifh;
  my $dim = @coords;
  print $ofh <<ENDTEXT;
NAME : $file
TYPE : TSP
DIMENSION : $dim
EDGE_WEIGHT_TYPE : EUC_2D
NODE_COORD_SECTION
ENDTEXT
  my $i = 1;
  foreach my $coord (@coords) {
    chomp $coord;
    my ($x,$y) = split(",", $coord);
    print $ofh "$i $x $y\n";
  } continue {
    $i++;
  }
  print $ofh "EOF\n";
  print $opfh <<ENDTEXT;
PROBLEM_FILE = $file.lkh.tsp
RUNS = 10
OUTPUT_TOUR_FILE = $file.lkh.res
ENDTEXT
}

