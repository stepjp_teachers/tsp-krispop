Prerequisites
=============

Requires that you have LKH installed and either in your path or at a
location set in an LKH environment variable.
You can get LKH from http://www.akira.ruc.dk/~keld/research/LKH/
and build by simply running make.

Usage
=====

Just run "./dolkh.pl <input1> [input2] ..."

For example:

    ./dolkh.pl input*.csv