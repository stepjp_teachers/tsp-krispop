#!/usr/bin/perl

use strict;
use Fatal qw(open close);
local $\="\n";

foreach my $file (@ARGV) {
  open(my $ifh, $file);
  my ($level) = $file=~m/input_(\d+)/;
  open(my $ofh, ">solution_yours_$level.csv");
  until (<$ifh>=~m/^TOUR_SECTION/) {
  }
  print $ofh "index";
  while (<$ifh>) {
    last if $_ < 0;
    print $ofh ($_ - 1);
  }
}

